# Python example of Mojave color scheme

import datetime as dt


class PythonExample:

    def __init__(self):
        self.name = "Python Mojave Color Scheme"
        x = 1 + 3

if __name__ == '__main__':
    pe = PythonExample()
    print '{} example on {}'.format(pe.name, dt.datetime.today())
