Mojave
======

Mojave is a slightly modified version of [Tom Ryder's][4] [Sahara][2] colorscheme for Vim.  The
major change is a dark gray background instead of black.

I ported the colorscheme to other text editors/IDEs I use regularly:

* Visual Studio 2010
* PyCharm
* SublimeText 2 (compatible with TextMate)
* Qt Creator (file goes in ~/.config/QtProject/qtcreator/styles/ on Linux)

License
-------

Copyright (c) [John Woltman][3], based on work by [Tom Ryder][4] and the original [Desert256][1] scheme. Distributed under the same terms as Vim itself.
See `:help license`.

[1]: http://www.vim.org/scripts/script.php?script_id=1243
[2]: http://sanctum.geek.nz/arabesque/sahara-vim-colorscheme/
[3]: https://woltman.com/
[4]: https://sanctum.geek.nz/

